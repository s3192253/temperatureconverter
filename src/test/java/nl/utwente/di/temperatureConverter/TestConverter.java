package nl.utwente.di.temperatureConverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** * Tests the Converter. */
public class TestConverter {
    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double celsius = converter.getFahrenheitFromCelsius(25.0);
        Assertions.assertEquals(72.0, celsius, 0.0, "25C to F");
    }
}
