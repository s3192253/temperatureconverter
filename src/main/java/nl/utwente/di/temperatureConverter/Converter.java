package nl.utwente.di.temperatureConverter;

public class Converter {
    public double getFahrenheitFromCelsius(double celsius) {
        return celsius * 9.0 / 5.0 + 32;
    }
}
